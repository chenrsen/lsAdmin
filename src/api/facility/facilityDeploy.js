import fetch from 'utils/fetch';
import { dev_ls, search_api } from 'api/config';

export function pubInfo(query) {
  return fetch({
    url: search_api + '/pubInfo/getPubInfo',
    method: 'get',
    data: query
  });
}

export function page(query) {
  return fetch({
    url: dev_ls + '/device/getPageList',
    method: 'post',
    data: query
  });
}

export function addObj(obj) {
  return fetch({
    url: dev_ls + '/device/addDevice',
    method: 'post',
    data: obj
  });
}

export function getObj(id) {
  return fetch({
    url: dev_ls + '/product/getProductDetail/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return fetch({
    url: dev_ls + '/product/delProduct/' + id,
    method: 'post'
  })
}


export function putObj(id, obj) {
  return fetch({

    method: 'put',
    data: obj
  })
}
