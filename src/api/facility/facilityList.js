import fetch from 'utils/fetch';
import { dev_ls, search_api } from 'api/config';

// 查询公众号接口
export function pubInfo(query) {
  return fetch({
    url: search_api + '/pubInfo/getPubInfo',
    method: 'get',
    data: query
  });
}

// 设备分页查询接口
export function page(query) {
  return fetch({
    url: dev_ls + '/device/getPageList',
    method: 'post',
    data: query
  });
}

// 新增设备接口
export function addObj(obj) {
  return fetch({
    url: dev_ls + '/device/addDevice',
    method: 'post',
    data: obj
  });
}

// 根据设备id查询设备信息接口
export function getObj(id) {
  return fetch({
    url: dev_ls + '/device/getDeviceInfoById/' + id,
    method: 'get'
  })
}

// 删除设备接口
export function delObj(id) {
  return fetch({
    url: dev_ls + '/device/delDevice/' + id,
    method: 'post'
  })
}


export function putObj(id, obj) {
  return fetch({
    url: dev_ls + '/device/delDevice/' + id,
    method: 'put',
    data: obj
  })
}
