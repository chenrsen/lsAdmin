import fetch from 'utils/fetch';
import { base_api } from 'api/config';

// 分页查询广告位
export function page(query) {
  return fetch({
    url: base_api + '/advertis_space/query',
    method: 'post',
    data: query
  });
}

// 新增广告位
export function addObj(obj) {
  return fetch({
    url: base_api + '/advertis_space/insert',
    method: 'post',
    data: obj
  });
}

export function getObj(id) {
  return fetch({
    url: base_api + '/product/getProductDetail/' + id,
    method: 'get'
  })
}

// 删除广告位接口
export function delObj(id) {
  return fetch({
    url: base_api + '/advertis_space/delete?id=' + id,
    method: 'get'
  })
}


