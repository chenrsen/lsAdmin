import fetch from 'utils/fetch';
import { base_api, see_api, dev_ls } from 'api/config';

// 分页查询广告位
export function advertPage() {
  return fetch({
    url: base_api + '/advertis_space/list',
    method: 'get'
  });
}

// 分页查询广告
export function page(query) {
  return fetch({
    url: base_api + '/advertis/query',
    method: 'post',
    data: query
  });
}

// 根据广告位id分页查询设备接口
export function getAdDevicePageList(obj) {
  return fetch({
    url: dev_ls + '/device/getAdDevicePageList',
    method: 'post',
    data: obj
  });
}

// 添加广告接口
export function addObj(obj) {
  return fetch({
    url: base_api + '/advertis/insert',
    method: 'post',
    data: obj
  });
}

// 更新广告
export function putObj(obj) {
  return fetch({
    url: base_api + '/advertis/update',
    method: 'post',
    data: obj
  });
}

export function getObj(id) {
  return fetch({
    url: base_api + '/advertis/selectOne?id=' + id,
    method: 'get'
  })
}

// 删除广告接口
export function delObj(id) {
  return fetch({
    url: base_api + '/advertis/delete?id=' + id,
    method: 'get'
  })
}

// 选择设备添加广告接口
export function getOtherPage(query) {
  return fetch({
    url: see_api + '/device/getOtherPage',
    method: 'post',
    data: query
  });
}

// 判断设备是否有添加广告接口
export function judgeDeviceAd(query) {
  return fetch({
    url: see_api + '/device/judgeDeviceAd',
    method: 'post',
    data: query
  });
}

// 确认添加广告
export function sureSelect(query) {
  return fetch({
    url: see_api + '/device/sureSelect',
    method: 'post',
    data: query
  });
}