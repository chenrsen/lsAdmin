import fetch from 'utils/fetch';
import { dev_api } from 'api/config';

export function page(query) {
  return fetch({
    url: dev_api + '/admin/groupType/page',
    method: 'get',
    params: query
  });
}

export function addObj(obj) {
  return fetch({
    url: dev_api + '/admin/groupType',
    method: 'post',
    data: obj
  });
}

export function getObj(id) {
  return fetch({
    url: dev_api + '/admin/groupType/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return fetch({
    url: dev_api + '/admin/groupType/' + id,
    method: 'delete'
  })
}

export function putObj(id, obj) {
  return fetch({
    url: dev_api + '/admin/groupType/' + id,
    method: 'put',
    data: obj
  })
}
