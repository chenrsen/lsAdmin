import fetch from 'utils/fetch';
import { dev_api } from 'api/config';

export function page(query) {
  return fetch({
    url: dev_api + '/admin/gateLog/page',
    method: 'get',
    params: query
  });
}

export function addObj(obj) {
  return fetch({
    url: dev_api + '/admin/gateLog',
    method: 'post',
    data: obj
  });
}

export function getObj(id) {
  return fetch({
    url: dev_api + '/admin/gateLog/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return fetch({
    url: dev_api + '/admin/gateLog/' + id,
    method: 'delete'
  })
}

export function putObj(id, obj) {
  return fetch({
    url: dev_api + '/admin/gateLog/' + id,
    method: 'put',
    data: obj
  })
}
