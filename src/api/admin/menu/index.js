import fetch from 'utils/fetch';
import { dev_api } from 'api/config';

export function fetchTree(query) {
  return fetch({
    url: dev_api + '/admin/menu/tree',
    method: 'get',
    params: query
  });
}

export function fetchAll() {
  return fetch({
    url: dev_api + '/admin/menu/all',
    method: 'get'
  });
}
export function addObj(obj) {
  return fetch({
    url: dev_api + '/admin/menu',
    method: 'post',
    data: obj
  });
}

export function getObj(id) {
  return fetch({
    url: dev_api + '/admin/menu/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return fetch({
    url: dev_api + '/admin/menu/' + id,
    method: 'delete'
  })
}

export function putObj(id, obj) {
  return fetch({
    url: dev_api + '/admin/menu/' + id,
    method: 'put',
    data: obj
  })
}
