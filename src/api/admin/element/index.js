import fetch from 'utils/fetch';
import { dev_api } from 'api/config';

export function page(query) {
  return fetch({
    url: dev_api + '/admin/element/list',
    method: 'get',
    params: query
  });
}

export function addObj(obj) {
  return fetch({
    url: dev_api + '/admin/element',
    method: 'post',
    data: obj
  });
}

export function getObj(id) {
  return fetch({
    url: dev_api + '/admin/element/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return fetch({
    url: dev_api + '/admin/element/' + id,
    method: 'delete'
  })
}

export function putObj(id, obj) {
  return fetch({
    url: dev_api + '/admin/element/' + id,
    method: 'put',
    data: obj
  })
}
