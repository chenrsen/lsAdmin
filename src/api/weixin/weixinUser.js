import fetch from 'utils/fetch';
import {search_api, dev_ls} from 'api/config';

//微信订阅号分页接口
export function pubInfo(query) {
  return fetch({
    url: search_api + '/pubInfo/getPageList',
    method: "post",
    data: query
  })
}

// 删除微信订阅号接口
export function delPubInfo(id) {
  return fetch({
    url: search_api + '/pubInfo/delPubInfo/' + id,
    method: 'post'
  })
}


//微信用户分页接口
export function listUser(query) {
  return fetch({
    url: search_api + '/weixin_user/getPageList',
    method: "post",
    data: query
  })
}

// 删除微信用户接口
export function delObj(id) {
  return fetch({
    url: search_api + '/weixin_user/delUserInfo/' + id,
    method: 'post'
  })
}

