import fetch from 'utils/fetch';
import { dev_ls } from 'api/config';


export function page(query) {
  return fetch({
    url: dev_ls + '/product/getPageList',
    method: 'get',
    data: query
  });
}

export function addObj(obj) {
  return fetch({
    url: dev_ls + '/product/saveProductInfo',
    method: 'post',
    data: obj
  });
}

export function getObj(id) {
  return fetch({
    url: dev_ls + '/product/getProductDetail/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return fetch({
    url: dev_ls + '/product/delProduct/' + id,
    method: 'post'
  })
}


export function putObj(id, obj) {
  return fetch({

    method: 'put',
    data: obj
  })
}
