import fetch from 'utils/fetch';
import { facility_api } from 'api/config';



// 获取设备数据接口
export function page(query) {
  return fetch({
    url: facility_api + '/statistics/equip',
    method: 'post',
    data: query
  });
}

//获取导出报表设备数据接口
export function pageExcel(query) {
  return fetch({
    url: facility_api + '/advertis/export/equip',
    method: 'post',
    data: query
  });
}


//获取公众号数据接口
export function vipcn(query) {
  return fetch({
    url: facility_api + '/statistics/vipcn',
    method: 'post',
    data: query
  });
}


//
// export function addObj(obj) {
//   return fetch({
//     url: dev_ls + '/product/saveProductInfo',
//     method: 'post',
//     data: obj
//   });
// }
