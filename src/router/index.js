import Vue from 'vue';
import Router from 'vue-router';

const _import = require('./_import_' + process.env.NODE_ENV);
// in development env not use Lazy Loading,because Lazy Loading large page will cause webpack hot update too slow.so only in production use Lazy Loading

Vue.use(Router);

/* layout */
import Layout from '../views/layout/Layout';

/**
 * icon : the icon show in the sidebar
 * hidden : if `hidden:true` will not show in the sidebar
 * redirect : if `redirect:noredirect` will no redirct in the levelbar
 * noDropdown : if `noDropdown:true` will has no submenu
 * meta : { role: ['admin'] }  will control the page role
 **/
export const constantRouterMap = [{
  path: '/login',
  component: _import('login/index'),
  hidden: true
},
{
  path: '/authredirect',
  component: _import('login/authredirect'),
  hidden: true
},
{
  path: '/404',
  component: _import('error/404'),
  hidden: true
},
{
  path: '/401',
  component: _import('error/401'),
  hidden: true
},
{
  path: '/',
  component: Layout,
  redirect: '/dashboard',
  name: '首页',
  hidden: true,
  children: [{
    path: 'dashboard',
    component: _import('dashboard/index')
  }]
},
{
  path: '/introduction',
  component: Layout,
  redirect: '/introduction/index',
  icon: 'form',
  noDropdown: true,
  children: [{
    path: 'index',
    component: _import('introduction/index'),
    name: '简述'
  }]
},
{
  path: '/adminSys/productManager',
  component: Layout,
  name: '产品管理',
  redirect: '/productManager/productList',
  icon: 'setting',
  authority: 'productManager',
  children: [{
    path: '/productManager/productList',
    component: _import('product/list/index'),
    name: '产品列表',
    authority: 'productList'
  }, {
    path: '/productManager/productFound',
    component: _import('product/found/index'),
    name: '创建产品',
    authority: 'productFound'
  }, {
    path: '/productManager/productDetails',
    component: _import('product/details/index'),
    name: '产品详情',
    authority: 'productDetails'
  }
  ]
}
]

export default new Router({
  // mode: 'history', //后端支持可开
  scrollBehavior: () => ({
    y: 0
  }),
  routes: constantRouterMap
});

export const asyncRouterMap = [{
  path: '/baseManager',
  component: Layout,
  name: '基础配置管理',
  icon: 'setting',
  authority: 'baseManager',
  children: [{
    path: 'userManager',
    icon: 'fa-user',
    component: _import('admin/user/index'),
    name: '用户管理',
    authority: 'userManager'
  }, {
    path: 'menuManager',
    icon: 'category',
    component: _import('admin/menu/index'),
    name: '菜单管理',
    authority: 'menuManager'
  }, {
    path: 'groupManager',
    icon: 'group_fill',
    component: _import('admin/group/index'),
    name: '角色权限管理',
    authority: 'groupManager'
  }, {
    path: 'groupTypeManager',
    icon: 'fa-users',
    component: _import('admin/groupType/index'),
    name: '角色类型管理',
    authority: 'groupTypeManager'
  }, {
    path: 'gateLogManager',
    icon: 'viewlist',
    component: _import('admin/gateLog/index'),
    name: '操作日志管理',
    authority: 'gateLogManager'
  }]
},
{
  path: '/authManager',
  component: Layout,
  name: '基础配置管理',
  icon: 'setting',
  authority: 'authManager',
  children: [{
    path: 'serviceManager',
    component: _import('auth/service/index'),
    name: '用户管理',
    authority: 'serviceManager'
  }]
},
{
  path: '/monitorManager',
  component: Layout,
  name: '监控模块管理',
  icon: 'setting',
  authority: 'monitorManager',
  children: [{
    path: 'serviceEurekaManager',
    component: _import('monitor/eureka/index'),
    name: 'Eureka注册中心',
    authority: 'serviceEurekaManager'
  }, {
    path: 'serviceMonitorManager',
    component: _import('monitor/service/index'),
    name: '服务状态监控',
    authority: 'serviceMonitorManager'
  }, {
    path: 'serviceZipkinManager',
    component: _import('monitor/zipkin/index'),
    name: '服务状态监控',
    authority: 'serviceZipkinManager'
  }]
},

{
  path: '/weixin',
  component: Layout,
  name: '微信管理',
  icon: 'setting',
  authority: 'weixin',
  children: [{
    path: 'SubscriptionManager',
    component: _import('weixin/SubscriptionManager/index'),
    name: '订阅号管理',
    authority: 'SubscriptionManager'
  }, {
    path: 'weixinManager',
    component: _import('weixin/weixinManager/index'),
    name: '微信用户管理',
    authority: 'weixinManager'
  }
  ]
},
{
  path: '/count',
  component: Layout,
  name: '统计报表',
  icon: 'setting',
  authority: 'count',
  children: [{
    path: 'devicesCount',
    component: _import('count/devicesCount/index'),
    name: '设备数据统计',
    authority: 'devicesCount'
  }, {
    path: 'tencentCount',
    component: _import('count/tencentCount/index'),
    name: '公众号数据统计',
    authority: 'tencentCount'
  }
  ]
},
{
  path: '/advertising',
  component: Layout,
  name: '广告管理',
  icon: 'setting',
  authority: 'advertising',
  children: [{
    path: 'advertisingW',
    component: _import('advertising/advertisingW/index'),
    name: '广告位管理',
    authority: 'advertisingW'
  }, {
    path: 'advertisingT',
    component: _import('advertising/advertisingT/index'),
    name: '广告投放管理',
    authority: 'advertisingT'
  }
  ]
},
{
  path: '/facilityManage',
  component: Layout,
  name: '设备管理',
  icon: 'setting',
  authority: 'facilityManage',
  children: [{
    path: 'facilityList',
    component: _import('facility/list/index'),
    name: '设备列表',
    authority: 'facilityList'
  }, {
    path: 'facilityDeploy',
    component: _import('facility/deploy/index'),
    name: '设备配置',
    authority: 'facilityDeploy'
  }]
}];
